#!/usr/bin/env python

import os
import psycopg2

from http.server import HTTPServer, BaseHTTPRequestHandler

DB_HOST = os.getenv("POSTGRES_HOST")
DB_NAME = os.getenv('POSTGRES_DB')
DB_USER = os.getenv('POSTGRES_USER')
DB_PASSWORD = os.getenv('POSTGRES_PASSWORD')

class HandleRequests(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b'Ok')

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        data = (self.rfile.read(content_length)).decode('utf-8')
        save_request(data)
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b'Ok')

def db_connection():
    return psycopg2.connect(dbname=DB_NAME, user=DB_USER, 
                            password=DB_PASSWORD, host=DB_HOST)

def create_table():
    conn = db_connection()
    with conn:
        with conn.cursor() as curs:
            curs.execute("CREATE TABLE IF NOT EXISTS requests (request_data varchar)")
    conn.close()

def save_request(data):
    conn = db_connection()
    with conn:
        with conn.cursor() as curs:
            curs.execute('INSERT INTO requests VALUES (%s)', (data,))
            conn.commit()
    conn.close()

def run(server_class=HTTPServer, handler_class=HandleRequests):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()


if __name__ == "__main__":
    create_table()
    run()